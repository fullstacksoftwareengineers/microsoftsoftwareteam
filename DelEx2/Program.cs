﻿using System;
using System.Collections.Generic;

namespace DelEx2
{
    public delegate bool EligibleForAdmission(Student EligibleForAdmission);
    public class Student
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public string CourseOfStudy { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }

        public static void AdmitStudent(List<Student> lstStudent, EligibleForAdmission IsStudentEligible)
        {
            foreach (Student student in lstStudent)
            {
                if (IsStudentEligible(student))
                {
                    Console.WriteLine("Student {0} Admitted", student.Name);
                }
            }
        }
        class Program
        {
            static void Main()
            {
                Student std1 = new Student()
                {
                    ID = 001,
                    Name = "Chris",
                    Score = 250,
                    CourseOfStudy = "Biology",
                    Age = 17,
                    Gender = "Male",
                };
                Student std2 = new Student()
                {
                    ID = 155,
                    Name = "Kemi",
                    Score = 320,
                    CourseOfStudy = "Mathematics",
                    Age = 19,
                    Gender = "Female",
                };
                Student std3 = new Student()
                {
                    ID = 089,
                    Name = "Stone",
                    Score = 185,
                    CourseOfStudy = "Agric Economics",
                    Age = 22,
                    Gender = "Male",
                };
                Student std4 = new Student()
                {
                    ID = 124,
                    Name = "Rilwan",
                    Score = 300,
                    CourseOfStudy = "Computer Science",
                    Age = 15,
                    Gender = "Male",
                };
                Student std5 = new Student()
                {
                    ID = 102,
                    Name = "Rice",
                    Score = 202,
                    CourseOfStudy = "Physics",
                    Age = 20,
                    Gender = "Male",
                };
                Student std6 = new Student()
                {
                    ID = 207,
                    Name = "Rebeccah",
                    Score = 90,
                    CourseOfStudy = "Chemistry",
                    Age = 19,
                    Gender = "Female",
                };
                Student std7 = new Student()
                {
                    ID = 133,
                    Name = "Hannah",
                    Score = 222,
                    CourseOfStudy = "Linguistics",
                    Age = 18,
                    Gender = "Female",
                };
                Student std8 = new Student()
                {
                    ID = 111,
                    Name = "Kairo",
                    Score = 268,
                    CourseOfStudy = "Electrical Engineering",
                    Age = 20,
                    Gender = "Male",
                };
                Student std9 = new Student()
                {
                    ID = 066,
                    Name = "Christine",
                    Score = 366,
                    CourseOfStudy = "Metallurgy",
                    Age = 21,
                    Gender = "Female",
                };
                Student std10 = new Student()
                {
                    ID = 050,
                    Name = "Joyce",
                    Score = 79,
                    CourseOfStudy = "Theology",
                    Age = 16,
                    Gender = "Female",
                };
                List<Student> lstStudents = new List<Student>();
                lstStudents.Add(std1);
                lstStudents.Add(std2);
                lstStudents.Add(std3);
                lstStudents.Add(std4);
                lstStudents.Add(std5);
                lstStudents.Add(std6);
                lstStudents.Add(std7);
                lstStudents.Add(std8);
                lstStudents.Add(std9);
                lstStudents.Add(std10);

                EligibleForAdmission eligibleForAdmission = new EligibleForAdmission(Program.Admit);
                Student.AdmitStudent(lstStudents, eligibleForAdmission);

                Console.ReadKey();
            }
            public static bool Admit(Student student)
            {
                if (student.Score > 200)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
