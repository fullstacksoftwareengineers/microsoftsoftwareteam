﻿

using System;
using System.Collections.Generic;
using Iteration.Practice;

namespace Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            //(2 -> 4 -> 3) + (5 -> 6 -> 4)Output: 7 -> 0 -> 8


            int[] linkedlistOneParams = { 2,4,3} , linkedListTwoParams = {5,6,4 };
            var linkedListOne = new LinkedList<int>(linkedlistOneParams);
            var linkedListTwo = new LinkedList<int>(linkedListTwoParams);

            //Unique Big Inter Sum using LinkedList:
            var solution = Algorithms.UniqueBigSums(linkedListOne, linkedListTwo);
            var linkedSolution = "";
            foreach (var item in solution)
            {

                linkedSolution +=" "+item;
            }

            Console.WriteLine("The sum of these two linked list are: {0}", linkedSolution);


            string list1 = "243", list2 = "564";

            //Unique Big Inter Sum using String:
            Console.WriteLine("The sum of these two linked list are: {0}", Algorithms.UniqueBigSums(list1, list2));

            //Palindrome Check:
            Console.WriteLine("Is a palindrome: {0}", Algorithms.PalindromeIndex("aaab"));

        Quadratic:
            try
            {
                Console.Write("Enter the value of a : ");
                var a = Convert.ToDouble(Console.ReadLine());

                Console.Write("Enter the value of b : ");

                var b = Convert.ToDouble(Console.ReadLine());

                Console.Write("Enter the value of c : ");

                var c = Convert.ToDouble(Console.ReadLine());
                var solve = Maths.MathsComputatitons.SolveQuadratic(a, b, c);
                Console.WriteLine("The roots of the quadric equation are: X1 = {0} and X2 = {1}", solve.Item1, solve.Item2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

            } 
            goto Quadratic;

            //Let us try a few declarations /Unity 3D - Java, C#, DirectX 9 -  C++... Physics


            int height;
            int weight;
            Console.WriteLine("Enter your height: ");
            height = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter your weight: ");
            weight = Convert.ToInt32(Console.ReadLine());

            int result = Sum(height, weight);
            int result2 = Subtract(height, weight);
            int result3 = Multiply(height, weight);
            int result4 = Divide(height, weight);
            int result5 = Quotient(height, weight);

            Console.WriteLine(result);
            Console.WriteLine(result2);
            Console.WriteLine(result3);
            Console.WriteLine(result4);
            Console.WriteLine(result5);
            Console.ReadLine();

           // System.Console.WriteLine(++height);//24

            //24 * 41
            //System.Console.WriteLine(height++ * ++weight);

            //25 + 41 = 66
            //System.Console.WriteLine(height + weight);

            //System.Console.Read();
        }

        public static int Sum(int a, int b)
        {

            return a + b;
        }
        public static int Subtract(int a, int b)
        {

            return a - b;
        }
        public static int Multiply(int a, int b)
        {

            return a * b;
        }
        public static int Divide(int a, int b)
        {

            return a / b;
        }

        public static int Quotient(int a, int b)
        {

            return a % b;
        }
    }
} 

