﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phone
{
  public class Phone
    {
      public string Brand { get; set; }
      public string Colour { get; set; }
      public string Battery { get; set; }
      public string Country { get; set; }

        public Phone()
        {
            Brand = "Huawei";
            Colour = "Black";
            Battery = "Detached";
            Country = "China";
        }

        /*public string GetBrand()
        {
            return Brand;
        }
        public void SetBrand(string brand)
        {
            Brand = brand;
        }
        public string GetBattery()
        {
            return Battery;
        }
        public string SetBattery(string battery)
        {
            return battery;
        }
        public string GetColour()
        {
            return Colour;
        }
        public string SetColour(string colour)
        {
            return colour;
        }
        public string GetCountry()
        {
            return Country;
        }
        public string SetCountry(string country)
        {
            return country;
        }*/
    }
}
