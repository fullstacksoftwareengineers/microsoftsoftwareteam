﻿using System;

namespace Nest
{
    public class ClassCar
    {
        private string condition = "Good";

        public string Condition
        {
            get
            {
                return condition;
            }
        }

        public class ClassCamry
        {
            public void ChangeCondition(ClassCar target, string newCondition)
            {
                target.condition = newCondition;
            }
        }

        class Program
        {
            static void Main(string[] args)
            {
                ClassCar myCar = new ClassCar() ;
                Console.WriteLine("myCar.Condition = {0}", myCar.Condition);
                ClassCar.ClassCamry myCamry = new ClassCar.ClassCamry();
                myCamry.ChangeCondition(myCar, "Excellent");
                Console.WriteLine("myCar.Condition = {0}", myCar.Condition);
                Console.ReadKey();
            }
        }
    }
}
