﻿using System;

namespace MathCalculation
{
    class Program
    {
        
        public static void Main(string[] args)
        {
            //finds the minimium and maximum of a set of numbers
            double x = 7.0, y = 7.6;
            //Power, Exponential and Log
            double b = 3.25, a = 2.50, newBase = 7.0;

            double j = -2;

            double l = +4.5, m = 0, n = -4.5;

            //conversion from degrees to radians
            double c = 30;
            double d = (c * (Math.PI))/180;
            Console.WriteLine(Math.Sin(d));
            Console.WriteLine("The Sine of {0} is {1}", c, Math.Sin(c));

            double g = 82;
            double h = (g * (Math.PI)) / 180;
            Console.WriteLine(Math.Cos(h));
            Console.WriteLine("The cosine of {0} is {1}", g, Math.Cos(g));

            double i = 225;
            Console.WriteLine("The tangent of {0} is {1}", i, Math.Tan(i));


            Console.WriteLine("The smaller of {0} and {1} is {2}.", x, y, Math.Min(x, y));
            Console.WriteLine("The greater of {0} and {1} is {2}.", x, y, Math.Max(x, y));

            Console.WriteLine("Exp ({0}) = {1}", a, Math.Exp(a));
            Console.WriteLine("Log ({0}) = {1}\n", a, Math.Exp(a), Math.Log(Math.Exp(a)));

            Console.WriteLine("Pow({0}, {1}) = {2}", b, a, Math.Pow(b, a));
            Console.WriteLine("Log10({0})/Log10({1}) = {2}\n", Math.Pow(b, a), b, Math.Log10(Math.Pow(b, a)) / Math.Log10(b));

            Console.WriteLine("Log{0}({1}) = {2}", newBase, a, Math.Log(a, newBase));
            Console.WriteLine("Pow({0}, {1}) = {2}", newBase, Math.Log(a, newBase), Math.Pow(newBase, Math.Log(a, newBase)));

            //Determines the modulus of a real number
            Console.WriteLine("Before:{0, -5} After: {1, -5}", j, Math.Abs(j));

            //Extracts the sign of a real number
            Console.WriteLine("The sign of {0} is {1}", l, Math.Sign(l));
            Console.WriteLine("The sign of {0} is {1}", m, Math.Sign(m));
            Console.WriteLine("The sign of {0} is {1}", n, Math.Sign(n));

        }
        /*public static void MultDivRem_Example()
        {
            int int1 = Int32.MaxValue;
            int int2 = Int32.MaxValue;
            int intResult;
            long longResult;
            double divisor, doubleResult;

            longResult = Math.BigMul(int1, int2);
            Console.WriteLine("{0}*{1}={2}\n", int1, int2);

            intResult = Math.DivRem(int1, 2, out int2);
            Console.WriteLine("{0}/{1}={2}, with a remainder of {3}.", int1, 2, intResult, int2);

            String str = "The IEEE remainder of {0:e}/{1/:f} is {2:e}";
            divisor = 2.0;
            doubleResult = Math.IEEERemainder(Double.MaxValue, divisor);
            Console.WriteLine(str, Double.MaxValue, divisor, doubleResult);

            divisor = 3.0;
            doubleResult = Math.IEEERemainder(Double.MaxValue, divisor);
            Console.WriteLine(str, Double.MaxValue, divisor, doubleResult);
        }*/
    }

}