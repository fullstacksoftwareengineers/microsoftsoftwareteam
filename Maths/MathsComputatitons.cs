﻿using System; //import Math

namespace Maths
{
   public class MathsComputatitons
    {

        /// <summary>
        /// Compute Discriminate -  Bsqr - 4ac
        /// Find out if its a negative Root [Complex number]
        /// Split into the roots
        /// </summary>
        /// <returns>The numerator.</returns>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        /// <param name="c">C.</param>
        public static Tuple<string,string> SolveQuadratic(double a, double b, double c)
        {
            string x1 = string.Empty, x2 =string.Empty;

            var discriminant = Math.Pow(b, 2) - 4 * a * c;
            

            if(discriminant < 0)
            {
                x1 = (-b / 2 * a).ToString() + " + "+ string.Format("{0:0.00}", (Math.Sqrt(-discriminant) / 2 * a)) +"i";

                x2 = (-b / 2 * a).ToString() + " - " + string.Format("{0:0.00}", (Math.Sqrt(-discriminant) / 2 * a)) + "i";
            }
            //This handles the real roots
            else
            {
                 x1 = ((-b + Math.Sqrt(discriminant)) / 2 * a).ToString();
                 x2 = ((-b - Math.Sqrt(discriminant)) / 2 * a).ToString();
            }
            var result = Tuple.Create(x1, x2);

            return result;

        }
    }
}

